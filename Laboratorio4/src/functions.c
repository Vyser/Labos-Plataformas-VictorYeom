#include "functions.h"

double productoPunto (punto_3D A, punto_3D B){
  double dot;
  dot = (A.X*B.X)+(A.Y*B.Y)+(A.Z*B.Z); //Se utiliza la definición de producto punto
  return dot; //Retorna el valor de la operación
}

punto_3D productoCruz (punto_3D A, punto_3D B){
  punto_3D cross;
  cross.X = (A.Y*B.Z)-(B.Y*A.Z); //Se calcula el producto cruz por medio de un determinante
  cross.Y = (B.X*A.Z)-(A.X*B.Z);
  cross.Z = (A.X*B.Y)-(B.X*A.Y);
  return cross; //Retorna el vector del producto cruz
}

double distanciaEuclidiana (punto_3D A, punto_3D B){
  double dist;
  double sum;
  sum = ((A.X-B.X)*(A.X-B.X))+((A.Y-B.Y)*(A.Y-B.Y))+((A.Z-B.Z)*(A.Z-B.Z));
  dist = sqrt (sum); //Se utiliza la fórmula para obtener la distancia euclidiana entre dos puntos
  return dist; //Retorna la distancia calculada
}

double areaCuadrilatero (punto_3D array1 [N]) {
  punto_3D A = array1 [0]; //Se define cada punto como una entrada del array que entra como parámetro
  punto_3D B = array1 [1];
  punto_3D C = array1 [2];
  punto_3D D = array1 [3];
  double diag = distanciaEuclidiana (A,C); //Se obtiene una diagonal para determinar dos triángulos
  double AB = distanciaEuclidiana (A,B); //Se obtiene la longitud de los lados del cuadrilátero
  double BC = distanciaEuclidiana (B,C);
  double CD = distanciaEuclidiana (C,D);
  double DA = distanciaEuclidiana (D,A);
  double s1 = (BC+AB+diag)/2; //Se calculan los semiperímetros de los triangulos obtenidos
  double s2 = (DA+CD+diag)/2;
  double ar1 = sqrt (s1*(s1-AB)*(s1-BC)*(s1-diag)); //Se utiliza la fórmula de Herón para calcular el área de los triángulos
  double ar2 = sqrt (s2*(s2-DA)*(s2-CD)*(s2-diag));
  double area = ar1+ar2; //Se suman las dos áreas de los triángulos
  return area;
}

punto_3D centroide (punto_3D array1 [N]){
  punto_3D A = array1 [0]; //Se define cada punto como una entrada del array que entra como parámetro
  punto_3D B = array1 [1];
  punto_3D C = array1 [2];
  punto_3D D = array1 [3];
  punto_3D centro; //Se declara el centroide
  centro.X = (A.X+B.X+C.X+D.X)/N; //Se calcula cada entrada del centroide
  centro.Y = (A.Y+B.Y+C.Y+D.Y)/N;
  centro.Z = (A.Z+B.Z+C.Z+D.Z)/N;
  return centro; //Retorna el vector correspondiente al centroide
}
