#include "functions.h"

int main (int argc, char const *argv[]){
  punto_3D A = {1,1,1};
  punto_3D B = {2,2,2};
  punto_3D C = {1,2,1};
  punto_3D D = {2,1,1};
  punto_3D array [N] = {A, B, C, D};

  double dot = productoPunto (B,C);
  printf("El producto punto entre B y C tiene como resultado: %f\n\n", dot);

  punto_3D cross = productoCruz (A,D);
  printf("El producto cruz entre A y D es dado por el vector: (%f,%f,%f)\n\n", cross.X, cross.Y, cross.Z );

  double area = areaCuadrilatero (array);
  printf("El área del cuadrilátero ABCD es de: %f\n\n", area );

  punto_3D centro = centroide(array);
  printf("El centroide en la nube de puntos es dado por el vector:(%f, %f, %f)\n\n", centro.X, centro.Y, centro.Z );

  return 0;
}
