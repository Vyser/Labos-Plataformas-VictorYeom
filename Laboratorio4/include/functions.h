#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define N 4

typedef struct {
  double X;
  double Y;
  double Z;
} punto_3D;

double productoPunto (punto_3D A, punto_3D B);

punto_3D productoCruz (punto_3D A, punto_3D B);

double distanciaEuclidiana (punto_3D A, punto_3D B);

double areaCuadrilatero (punto_3D[4]);

punto_3D centroide (punto_3D[N]);
