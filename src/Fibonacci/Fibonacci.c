#include<stdio.h>
#include <stdlib.h>

int Fibo (int n) {

int num1 = 1;
int num2 = 1;
int num3 = 1;

for (int i=3; i<=n; i++) {
	num3=num2+num1;
	num1=num2;
	num2=num3;
}

return num3;
}

void main (int argc, char** argv) {

int num = atoi(argv[1]);
int res = Fibo(num);
printf ("El número es %i\n", res);

}
