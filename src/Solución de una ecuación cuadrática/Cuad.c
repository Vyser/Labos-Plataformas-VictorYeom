#include<stdio.h>
#include<math.h>

void cuad (float a, float b, float c) {

float dis = b*b-4*a*c;

if (dis>=0) {
float sol1 = (-b+sqrt(dis))/(2*a);
float sol2 = (-b-sqrt(dis))/(2*a);
printf("Las soluciones de la ecuación son %f y %f.\n", sol1, sol2);
}

if (dis<0) {

printf("La ecuación no tiene soluciones reales.\n");
}

}

void main (int argc, char** arv) {

cuad (4,5,1);
cuad (4,5,6);

}
