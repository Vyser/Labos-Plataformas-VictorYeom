//Importo las bibliotecas que voy a usar en el programa. Cualquier .c que importe Normalidad.h tendrá estas bibliotecas disponibles.
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

//Declaro las funciones que voy a hacer para resolver el problema.
int fileLength (const char* path);  //Función que me devuelve la cantidad de elementos en un archivo de texto generado en Python
int load(const char* path, double** dir); //Función para cargar arrays
double mean (double* sample, int size); //Función para calcular la media
double deviation (double* sample, double mean, int size); //Función para calcular la desviación estándar
double thirdMoment (double* sample, double mean, int size); //Función para calcular el tercer momento normal
double fourthMoment (double* sample, double mean, int size); //Función para calcular el cuarto momento normal
