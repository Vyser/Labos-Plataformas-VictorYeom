#include "Normalidad.h"

int main (int argc, char** argv){
  char* path = argv[1]; //Guardo la variable que entra como parámetro en la línea de comando (el path del archivo)
  int cant = fileLength(path); //Veo cuántos elementos tiene el archivo de texto con la muestra
  double* dir; //Creo un puntero (array de mem. dinámica) con un tamaño tal que pueda guardar todos los elementos
  load(path, &dir); //Cargo el array con los contenidos del archivo
  double m = mean (dir, cant); //Calculo la media de la muestra
  double d = deviation (dir, m, cant); //Calculo la desviación estándar de la muestra
  double m3 = thirdMoment (dir, m, cant); //Calculo el tercer momento de la muestra
  double m4 = fourthMoment (dir, m, cant); //Calculo el cuarto momento de la muestra
  //Imprimo todos los datos
  printf("Media: %f\n", m);
  printf("Desviación estándar: %f\n", d);
  printf("Tercer momento: %f\n", m3);
  printf("Cuarto momento:%f\n", m4);
  free (dir); //Libero la memoria asociada al puntero
  return 0;
}
