#include "Normalidad.h"

//Función para definir la cantidad de elementos en un archivo de texto plano
int fileLength (const char* path){
  int ctr = 0;
  FILE* fp;
  char buff[255];

  fp = fopen(path, "r");

  char* ptr = fgets(buff, 255, fp);

  while (ptr != NULL){
    ctr++;
    ptr = fgets(buff, 255, fp);
  }
  fclose(fp);
  return ctr;
}

int load (const char* path, double** dir){
  int ctr = 0; //Variable contador para ver cuántos elementos hay
  FILE* fp; //Puntero para abrir un archivo de texto
  char buff[255]; //Buffer para guardar cada secuencia de chars que representa un número
  *dir = malloc(sizeof(double)*fileLength(path));

  fp = fopen(path, "r"); //Abro el archivo de texto en modo de lectura

  char* ptr = fgets(buff, 255, fp); //Leo una String hasta el cambio de línea, guardo el contenido de la línea en el buffer y paso a la siguiente

  if (ptr != NULL){ //Reviso que el texto no esté vacío
    (*dir)[ctr] = atof(buff); //Si no está vacío empiezo a guardar datos en el array.
    //El asterisco provee acceso a lo que se almacena en el puntero al que apunta dir ("desreferencia" la variable).
  }

  while (ptr != NULL){ //Mientras no haya alcanzado el final del archivo, hago lo siguiente:
    ctr++; //Le subo uno al contador (ya que se tuvo que haber leído al menos un elemento del archivo)
    ptr = fgets(buff, 255, fp); //Hago lo mismo que en la línea 10 en la línea que seguía
    if (ptr != NULL){ //Reviso que el elemento no sea vacío
      (*dir)[ctr] = atof(buff); //Si no es vacío guardo el siguiente dato en el array
    }
  }

  fclose(fp); //Cierro el archivo
  return ctr; //Devuelvo la cantidad de elementos en el archivo
}

//Función para calcular la media de una muestra
double mean (double* sample, int size){ //Entran de parámetros el array con los datos de la muestra y el tamaño de la muestra
  double mean = 0; //Declaro e inicializo la variable de media para manipularla luego

  //Utilizo la fórmula para calcular la media de una muestra
  for (int i=0; i<size; i++){ //El for se utiliza como la sumatoria. Recorre desde el primer elemento de la muestra hasta el último
    mean += sample[i]; //Voy sumando elemento por elemento
  }
  mean /= size; //Divido entre el tamaño total de la muestra y obtendo la media
  return mean; //Retorno el valor de la media muestral
}

//Función para calcular la desviación estándar de una muestra
double deviation (double* sample, double mean, int size){ //Recibe de parámetros el archivo con los datos de la muestra, la media y el tamaño de la misma
  double dev = 0; //Declaro e inicializo la variable para manipularla

  //Utilizo la fórmula para calcular desviación estándar
  for (int i=0; i<size; i++){ //El for es la sumatoria
    dev += (sample[i]-mean)*(sample[i]-mean); //Voy sumando el valor de cada elemento menos la media al cuadrado
  }

  dev /= size; //Divido el valor de la sumatoria entre el tamaño de la muestra por fórmula
  dev = sqrt(dev); //Le saco raíz para obtener la desviación estándar
  return dev; //Retorno el valor de la desviación
}

//Función para determinar el tercer momento de una muestra
double thirdMoment (double* sample, double mean, int size){ //Recibe de parámetros el archivo con la muestra, la media y el tamaño de la misma
  //Declaro las variables para usarlas luego
  double m3 = 0;
  double m2 = 0;
  double moment;

  // Utilizo la fórmula para obtener el tercer momento
  for (int i=0; i<size; i++){ //for para sumatoria
    //Uso la fórmula para calcular las m (fórmula 5 en el enunciado del examen)
    m3 += pow(sample[i]-mean, 3); //La función pow toma el primer parámetro y le mete el segundo parámetro como exponente
    m2 += pow(sample[i]-mean, 2);
  }
  //Por fórmula divido cada sumatoria por el tamaño de la muestra
  m3 /= size;
  m2 /= size;

  //Uso la fórmula (3) del enunciado del examen
  moment = m3/pow(m2,3/2);
  return moment; //Retorno el valor del tercer momento
}

//Función para determinar el cuarto momento de una muestra
double fourthMoment (double* sample, double mean, int size){ //Recibe como parámetros el archivo con la muestra, su media y su tamaño
  //Declaro las variables para manipularlas luego
  double m4 = 0;
  double m2 = 0;
  double moment;

  //Utilizo fórmula para obtener el momento
  for (int i=0; i<size; i++){ //for para sumatoria
    m4 += pow(sample[i]-mean, 4); //Uso la fórmula (5) del enunciado para calcular cada m
    m2 += pow(sample[i]-mean, 2);
  }
  //Por fórmula divido cada m por el tamaño de la muestra
  m4 /= size;
  m2 /= size;

  moment = m4/pow(m2, 2); //Uso fórmula (4) para calcular el cuarto momento
  return moment; //Retorno el valor del cuarto momento
}
