#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy.random as random
from matplotlib import pyplot as plt

def main():
    # Utilizo la función numpy.random.binomial para generar muestras aleatorias con una distribución binomial
    o1 = random.binomial(20, 0.166667, size=15) # Ejemplo tomado de Lacey, M. n=20, p=0.166667
    o2 = random.binomial(20, 0.166667, size=500)

    # Genero los archivos txt para usarlos en C
    writeSample(o1, '../C/text/o1.txt')
    writeSample(o2, '../C/text/o2.txt')

    # Genero los histogramas con cada una de las muestras. Para las muestras más pequeñas
    # pongo None para que la función decida la cantidad de bins que considera óptima
    histogram(o1, 7, 'o1')
    histogram(o2, 20, 'o2')

# Defino la función para escritura de archivos txt, recibirá la muestra a escribir y el path del archivo.
# El archivo tendrá el nombre que se especifique en el path
def writeSample (sample, file):
    f = open (file,'w') # Abro el archivo en modo de escritura
    row = len(sample)
    for i in range(row): # Loop para recorrer toda la muestra
        f.write (str(sample [i])+'\n') # Empiezo a escribir la muestra elemento por elemento,
        # separando cada uno con cambio de línea
    f.close # Cierro el archivo

# Defino la función para generar los histogramas de distribución binaria. Recibe de parámetros
# la muestra a graficar, la cantidad de bins deseada y el nombre del archivo png
def histogram (sample, bin, name):
    plt.hist(sample, bins=bin) # Genera el histograma
    plt.suptitle('%s' %name) # Le pongo título al histograma para identificarlo

    # Envío la imagen con el histograma al path especificado y le pongo el nombre que entra como parámetro
    plt.savefig('../../Imágenes/Python/%s.png' % name)
    plt.close() # Cierro la figura

if __name__ == '__main__':
    main()
