#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy.random as random
from matplotlib import pyplot as plt

def main():
    # Utilizo la función numpy.random.laplace para generar muestras aleatorias con una distribución de Laplace
    o5 = random.laplace(size=15) # La función por default usa una media igual a 0 y b=1, no hace falta especificarlos.
    o6 = random.laplace(size=500)

    # Genero los archivos txt para usarlos en C
    writeSample(o5, '../C/text/o5.txt')
    writeSample(o6, '../C/text/o6.txt')

    # Genero los histogramas con cada una de las muestras. Para las muestras más pequeñas
    # pongo None para que la función decida la cantidad de bins óptima
    histogram(o5, 7, 'o5')
    histogram(o6, 100, 'o6')

# Defino la función para escritura de archivos txt, recibirá la muestra a escribir y el path del archivo.
# El archivo tendrá el nombre que se especifique en el path
def writeSample (sample, file):
    f = open (file,'w') # Abro el archivo en modo de escritura
    row = len(sample)
    for i in range(row): # Loop para recorrer toda la muestra
        f.write (str(sample [i])+'\n') # Empiezo a escribir la muestra elemento por elemento,
        # separando cada uno con cambio de línea
    f.close # Cierro el archivo

# Defino la función para generar los histogramas de distribución de Laplace. Recibe de parámetros
# la muestra a graficar, la cantidad de bins deseada y el nombre del archivo png
def histogram (sample, bin, name):
    plt.hist(sample, bins=bin) # Genera el histograma
    plt.suptitle('%s' %name) # Le pongo título al histograma para identificarlo

    # Envío la imagen con el histograma al path especificado y le pongo el nombre que entra como parámetro
    plt.savefig('../../Imágenes/Python/%s.png' % name)
    plt.close() # Cierro la figura

if __name__ == '__main__':
    main()
