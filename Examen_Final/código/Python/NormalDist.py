#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Importo los módulos que voy a usar
import numpy.random as random
from matplotlib import pyplot as plt

def main():
    # Utilizo la función numpy.random.normal para generar muestras aleatorias con una distribución normal
    # con los parámetros solicitados
    m1 = random.normal(loc=0, scale=1, size=10)
    m2 = random.normal(loc=0, scale=1, size=100)
    m3 = random.normal(loc=0, scale=1, size=1000)
    m4 = random.normal(loc=5, scale=3, size=100)
    m5 = random.normal(loc=5, scale=3, size=1000)

    # Genero los archivos txt para usarlos en C
    writeSample(m1, '../C/text/m1.txt') # Genero los archivos desde una carpeta y quiero mandarla a otra
    writeSample(m2, '../C/text/m2.txt') # Las carpetas serán subidas a gitlab. Si se desea se pueden cambiar los paths
    writeSample(m3, '../C/text/m3.txt')
    writeSample(m4, '../C/text/m4.txt')
    writeSample(m5, '../C/text/m5.txt')

    # Genero los histogramas con cada una de las muestras. Para las muestras más pequeñas
    # pongo None para que la función decida la cantidad de bins que considera óptima
    histogram(m1, 5, 'm1')
    histogram(m2, 15, 'm2')
    histogram(m3, 60, 'm3')
    histogram(m4, 15, 'm4')
    histogram(m5, 60, 'm5')

# Defino la función para escritura de archivos txt, recibirá la muestra a escribir y el path del archivo.
# El archivo tendrá el nombre que se especifique en el path
def writeSample (sample, file):
    f = open (file,'w') # Abro el archivo en modo de escritura
    row = len(sample)
    for i in range(row): # Loop para recorrer toda la muestra
        f.write (str(sample [i])+'\n') # Empiezo a escribir la muestra elemento por elemento,
        # separando cada uno con cambio de línea
    f.close # Cierro el archivo

# Defino la función para generar los histogramas de distribución normal. Recibe de parámetros
# la muestra a graficar, la cantidad de bins deseada y el nombre del archivo png
def histogram (sample, bin, name):
    plt.hist(sample, bins=bin) # Genera el histograma
    plt.suptitle('%s' %name) # Le pongo título al histograma para identificarlo

    # Envío la imagen con el histograma al path especificado y le pongo el nombre que entra como parámetro
    plt.savefig('../../Imágenes/Python/%s.png' % name)
    plt.close() # Cierro la figura

if __name__ == '__main__':
    main()
