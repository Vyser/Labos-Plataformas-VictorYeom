#include "lista.h"

pos_t* createList (int first_value){

  pos_t* first = malloc (sizeof(pos_t));
  first->data = first_value;
  first->next = 0;
  return first;

}

void printList (pos_t* head){
  pos_t* inter = head;
  while (inter->next!=0){ //Reviso si estoy en el último elemento de la lista
    printf("%d ", inter->data); //Imprimo el dato del elemento en el que estoy
    inter = inter->next; //Como aún no estoy en el último elemento, avanzo 1
  }

  if (inter->next==0){ //Llego al último elemento de la lista
    printf("%d\n", (*inter).data); //Imprimo el conenido del elemento
  }
}

int push_back (pos_t* head, int new_value){

  int res;

  while (head->next!=0){
    head = head->next; //Llego al último elemento de la lista
  }

  if (head->next==0){ //Reviso si estoy en el último
    pos_t* inter = malloc (sizeof(pos_t)); //Asigno memoria necesarua para el encabezado

    if (inter==0) {res=0;} //Si no logré apartar memoria devuelvo 0

    else if (inter!=0){ //Logré apartar memoria
      head->next = inter; //Enlazo el último elemento de la lista al nuevo
      inter->data = new_value; //Asigno el valor que tiene el nuevo elemento
      inter->next = 0; //El nuevo elemento es el último
      res=1;
    }
  }
  return res; //Me dice si logré apartar memoria o no
}

int push_front (pos_t** head, int new_value){

  int res;

  pos_t* inter = malloc(sizeof(pos_t)); //Intento apartar memoria

  if (inter==0){res=0;} //Si no logro apartar memoria devuelvo 0

  else{
    inter->next = *head; //El nuevo elemento está al inicio, lo enlazo a la cabeza
    inter->data = new_value; //Le asigno el valor al nuevo elemento
    *head=inter; //Hago que el nuevo elemento se vuelva la cabeza
    res=1;
  }
  return res;
}

int pop_back (pos_t* head){
  int res;

  if (head->next==0){ //Si la lista es de un solo elemento hago esto
    res = head->data; //Almaceno el valor del elemento en res
    free (head); //Libero la memoria
  }else{ //Si la lista tiene más de un elemento hago lo siguiente
    pos_t* curr=head; //Hago una variable intermedia
    while (curr->next->next !=0){ //Reviso si estoy en la penúltima posición
      curr = curr->next; //Si no estoy en la penúltima posición adelanto 1
    }
    res = curr->next->data; //Almaceno el valor del último elemento
    free (curr->next); //Libero la última posición de la lista
    curr->next=0; //El elemento en el que estoy se vuelve el último de la lista
  }
  return res;
}

int pop_front (pos_t** head){
  pos_t* inter = *head;
  int res = inter->data;

  if (inter->next!=0){ //Reviso si la lista es de 1 solo elemento
    *head=(*head)->next; //Si no lo es convierto el segundo elemento en la cabeza
    free (inter); //Libero la memoria del primer elemento
  } else{
    free (inter); //Si es de 1 solo elemento libero la memoria del único
  }
  return res;
}

int insert (pos_t** head, int pos, int new_value){
  int index = 0; //Índice que indica si se logró insertar o no
  int ctr=1; //Contador que inicia en 1
  pos_t* last=*head; //Variable para hallar el último elemento de la lista
  pos_t* inter=*head;

  while (last->next!=0){
    last = last->next; //Algoritmo para hallar el último elemento de la lista
    ctr++; //Cada vez que se adelanta 1 elemento en la lista se suma 1 a ctr
  }//Finalmente ctr tendrá un valor igual a la cantidad total de elementos más 1
  if (pos==0){ //Reviso si quiero insertar el elemento en la primera posición
    index = push_front(head, new_value);
  }
  if (pos==ctr) { //Reviso si quiero insertar el elemento en la última posición
    index = push_back(*head, new_value);
  }
  if ((pos < ctr) && (pos>0)){ //Reviso si quiero insertar el elemento en medio
    pos_t* new = malloc(sizeof(pos_t)); //Aparto memoria para el nuevo elemento "new"
    if (new!=0) {index=1;} //Reviso si logré apartar memoria
    for (int i=0; i<pos-1; i++){
      inter = inter->next; //Llego al elemento que está justo antes del nuevo
    }
    new->next = inter->next; //El nuevo elemento apunta al que apuntaba inter
    new->data = new_value; //El valor del nuevo elemento es asignado
    inter->next = new; //inter ahora apunta al elemento nuevo
  }

  if (pos > ctr) { //Reviso si la posición ingresada es válida
    printf("La posición que ha ingresado es inválida\n");
  }
  return index;
}

void removeElement (pos_t** head, int pos){
  int ctr=0; //Inicio el contador en 0
  pos_t* last = *head;
  pos_t* inter = *head;
  pos_t* prev = *head;

  while (last->next!=0){
    last = last->next; //Busco el último elemento de la lista
    ctr++;
  } //ctr tiene un valor igual a la cantidad total de elementos

  if (pos==0){ //Reviso si quiero eliminar el primer elemento
    pop_front(head);
  }

  if (pos==ctr) { //Reviso si quiero eliminar el último elemento
    pop_back(*head);
  }

  if ((pos<ctr)&&(pos>0)) { //Reviso si quiero eliminar un elemento en medio
    for (int i=0; i<pos-1; i++){ //Hago el ciclo hasta el elemento que me interesa
      inter = inter->next;
      prev = prev->next;
    } inter = inter->next; //Llego al elemento que quiero eliminar
    prev->next = inter->next; //Enlazo el elemento anterior al elemento siguiente
    free (inter); //Libero la posición del elemento a eliminar
  }

  if (pos>ctr) { //Reviso si la posición es inválida
    printf("La posición que ha ingresado es inválida\n");
  }
}

int freeList (pos_t* head){
  pos_t* next;
  pos_t* before=head;

  while (before->next!=0) { //Voy elemento por elemento hasta llegar al último
    next = before->next; //Agarro next para asignarle el valor del siguiente elemento
    free (before); //Libero el elemento anterior
    before = next; //Almaceno en la variable libre el valor actual de next
  } //Se liberan todas las posiciones hasta la penúltima

  if (before->next==0){
    free(before); //Libero el último elemento que queda
  }
  return 0;
}

pos_t* getElement (pos_t* head, int index){
  pos_t* last = head;
  int ctr = 0;

  while (last->next!=0){
    last = last->next; //Llego al último elemento de la lista
    ctr++;
  }

  if ((index>=0)&&(index<ctr)){ //Reviso si el índice está en el intervalo
    for (int i=0; i<index; i++){ //Hago el ciclo hasta llegar al elemento buscado
      head = head->next;
    }
    return head;
  }else {return 0;} //Si no encuentro el elemento retorno 0

}

pos_t* searchFor (pos_t* head, int data){
  pos_t* sol = 0;

  while (head->next!=0){ //Voy buscando elemento por elemento hasta el penúltimo
    if (head->data==data){ //Si el elemento coincide con lo que busco
      sol = head; //sol es elemento que me interesa hallar
    }
    if (sol!=NULL){break;} //Apenas encuentre la primera coincidencia termino el ciclo
    head = head->next;
  }

  if (head->data==data){ //Reviso si el último elemento coincide con lo buscado
    sol = head;
  }
  return sol;
}

void sort (pos_t* head, char dir){

  if (head!=0){ //Reviso si la lista es de un solo elemento
    pos_t* behind;
    pos_t* ahead;
    int index;

    if (dir == 'a'){ //Si quiero orden ascendente
      do {
        index = 1;
        behind = head; //Asigno una variable para ver el elemento a la izquierda
        ahead = head->next; //Variable para ver el elemento a la derecha
        while (behind->next!=0){ //Reviso que no esté en el último elemento
          if (behind->data > ahead->data){ //Comparo los elementos adyacentes
            int inter = behind->data; //Variable intermedia guarda el número mayor
            behind->data = ahead->data; //Muevo el número menor a la izquierda
            ahead->data = inter; //Muevo el número mayor a la derecha
            index = 0; //Como tuve que cambiar valores vuelvo a enciclar
          }
          behind = behind->next; //Me muevo un espacio
          ahead = ahead->next;
        }
      } while (index==0);
    }

    if (dir == 'd'){ //Si quiero orden descendente
      do {
        index = 1;
        behind = head;
        ahead = head->next;
        while (behind->next!=0){
          if (behind->data < ahead->data){ //La misma lógica que antes
            int inter = behind->data; //Solo cambia la dirección de comparación
            behind->data = ahead->data;
            ahead->data = inter;
            index = 0;
          }
          behind = behind->next;
          ahead = ahead->next;
        }
      } while (index==0);
    }

  }
}

pos_t* readList (const char* filePath){
  FILE* fp; //Preparo un puntero para leer el archivo
  pos_t* first;
  char buff[255]; //Preparo el buffer para lectura

  fp = fopen(filePath, "r"); //Abro el archivo en modo de lectura

  char* ptr = fgets(buff, 255, fp); //Inicializo el puntero
  int dat = atoi(buff); //Guardo el valor numérico del primer valor de la lista

  if (ptr!=0){ //Reviso que el archivo no sea nulo
    first = createList(dat); //Como no es nulo creo una lista con el primer valor
  }

  while(ptr != 0){ //Ciclo para leer el archivo, reviso si he llegado al final
    ptr = fgets(buff, 255, fp); //Asigno el valor que sigue en el archivo a ptr
    if (ptr!=0){ //Reviso si el valor que seguía era el final del archivo
      push_back(first, atoi(buff)); //Si no lo era agrego el elemento
    }
  }
  fclose(fp);
  return first; //Retorno el puntero que apunta al primer elemento de la lista
}

void writeList (pos_t* head, const char* filePath){
  FILE* fp; //Preparo variable para leer el archivo
  fp = fopen(filePath, "w"); //Abro el archivo en modo de escritura

  while (head->next!=0){ //Recorro elemento por elemento
    fprintf(fp, "%d\n", head->data); //Imprimo en el archivo el valor del elemento
    head = head->next; //Paso al siguiente elemento
  } //Imprimo hasta el penúltimo elemento

  if (head->next==0){
    fprintf(fp, "%d\n", head->data); //Imprimo el último elemento
  }

  fclose(fp);
}
