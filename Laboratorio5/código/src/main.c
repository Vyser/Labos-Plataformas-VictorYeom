#include "lista.h"

int main (int argc, char** argv){

  pos_t* t = createList (5);
  printf("*createList\n");
  printList (t);
  printf("\n");

  push_back(t,4);
  printf("*push_back\n");
  printList (t);
  printf("\n");

  push_front(&t,4);
  printf("*push_front\n");
  printList(t);
  printf("\n");

  /*Agrego algunos elementos para hacer la lista más grande*/
  push_back(t, 16);
  push_back(t, 192);
  push_front(&t, 1);
  printf("*Agrego algunos elementos a la lista\n");
  printList(t);
  printf("\n");

  pop_back(t);
  printf("*pop_back\n");
  printList(t);
  printf("\n");

  pop_front(&t);
  printf("*pop_front\n");
  printList(t);
  printf("\n");

  insert (&t, 1, 123);
  printf("*insert un elemento en el medio\n");
  printList(t);
  printf("\n");

  printf("*insert en posición inválida\n");
  insert (&t, 123123, 1);
  printf("\n");

  removeElement (&t, 2);
  printf("*removeElement un elemento en el medio\n");
  printList(t);
  printf("\n");

  printf("*removeElement en posición inválida\n");
  removeElement (&t, 123123);
  printf("\n");

  pos_t* test1 = getElement(t,1);
  printf("*Comprobación de getElement\n");
  printf("%d\n", test1->data);
  printf("\n");

  pos_t* test2 = searchFor(t, 16);
  printf("*Comprobación de searchFor\n");
  printf("%d\n", test2->data);
  printf("\n");

  sort(t, 'a');
  printf("*sort ascendente\n");
  printList(t);
  printf("\n");

  sort(t, 'd');
  printf("*sort descendente\n");
  printList(t);
  printf("\n");

  writeList(t, "prueba.txt");

  pos_t* s = readList("prueba.txt");
  printf("*Comprobación de readList\n");
  printList(s);

  freeList (s);
  freeList (t);
  return 0;
}
