#include <stdlib.h>
#include <stdio.h>

typedef struct pos {

  int data;

  struct pos* next;
} pos_t;

pos_t* createList (int first_value);

pos_t* readList (const char* filePath);

void writeList (pos_t* head, const char* filePath);

int push_back (pos_t* head, int new_value);

int push_front (pos_t** head, int new_value);

int pop_back (pos_t* head);

int pop_front (pos_t** head);

int insert (pos_t** head, int pos, int new_value);

void removeElement (pos_t** head, int pos);

int freeList (pos_t* head);

pos_t* getElement (pos_t* head, int index);

pos_t* searchFor (pos_t* head, int data);

void sort (pos_t* head, char dir);

void printList(pos_t* head);
