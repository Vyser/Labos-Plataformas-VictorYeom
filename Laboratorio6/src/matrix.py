import matplotlib.pyplot as plt
def main():

    k = [[23,35,12,23,16],[21,23,35,63,3]]
    d = [[0,1,0],[1,0,0],[0,0,-1]]
    e = [[1,0,2],[5,4,11],[12,5,23]]


    print("Lectura de matrices:")
    m = createMatrix("matrizPrueba.txt")
    print (m)
    n = createMatrix ("matrizPrueba2.txt")
    print(n)
    print('\n')

    writeMatrix (m, "pruebaEscritura.txt")

    print("Suma de matrices: ")
    a = matrixSum(n,n)
    print (a)
    matrixSum(n,m)
    print('\n')

    print ("Multiplicación de matrices: ")
    b = matrixMult(n,m)
    print (b)
    matrixMult(k,d)
    print('\n')

    print ("Determinante:  ")
    print (determinant (b))
    print (determinant (e))
    print('\n')

    print ("Reducción Gauss Jordan: caso con una fila 0")
    print (GaussJordan (b))
    print ("Reducción Gauss Jordan: caso equivalente a identidad")
    print (GaussJordan (e))
    print ("Reducción Gauss Jordan: caso matriz rectangular")
    print (GaussJordan (k))
    print('\n')

    print ("Matriz inversa: caso sin inversa")
    print (findInv (b))
    print ("Matriz inversa: caso con inversa")
    print (findInv (e))
    print('\n')

    print("Histograma: ")
    histogram (m, 5)

def createMatrix(file):
    f = open (file,'r')
    l = [[int(num) for num in line.split(' ')] for line in f ]
    f.close
    return l

def writeMatrix (matrix, file):
    f = open (file,'w')
    row = len(matrix)
    col = len(matrix[0])
    for i in range(row):
        for j in range(col):
            if j < col-1:
                f.write (str(matrix [i][j])+' ')
            if j == col-1:
                f.write (str(matrix [i][j])+'\n')
    f.close

def matrixSum (mat1, mat2):
    row1 = len(mat1)
    row2 = len(mat2)
    col1 = len(mat1[0])
    col2 = len(mat2[0])
    if (row1 == row2) and (col1 == col2): #Solo matrices con igual dimensión se suman
        res = []
        for i in range(row1):
            temp = []
            for j in range(col1):
                temp.append(mat1[i][j] + mat2[i][j])
            res.append(temp)
        return res
    else:
        print ("Las matrices no se pueden sumar")

def matrixMult (mat1, mat2):
    row1 = len(mat1)
    row2 = len(mat2)
    col1 = len(mat1[0])
    col2 = len(mat2[0])
    if (col1 == row2): #Verifico las dimensiones para multiplicar
        res = []
        for elem in range(row1):
            temp = []
            for i in range(col2):
                num = 0
                for j in range(row2):
                    num += mat1[elem][j]*mat2[j][i] #Implemento el algoritmo
                temp.append(num)
            res.append(temp)
        return res
    else:
        print ("Las matrices no se pueden multiplicar")

def histogram (mat, bin):
    res = []
    for i in range(len(mat)):
        for j in range(len(mat[0])):
            res.append(mat[i][j])
    plt.hist(res, bins=bin)
    plt.show()

def copiarMatriz (matriz):
    mat = []
    rows = len(matriz)
    cols = len(matriz[0])
    for i in range (rows):
        medRow = []
        for j in range (cols):
            medRow.append(matriz[i][j])
        mat.append(medRow)
    return mat

def GaussJordan (matriz):
    sum = 0 #Variable para ver si la matriz es 0
    index = 0 #Variable para ver si la matriz es rectangular
    rows = len(matriz)
    rowscopy = len(matriz) #Variable de control para rectangulares
    cols = len(matriz[0])

    for i in range(rows):
        for j in range(cols):
            sum += abs(matriz[i][j]) #Sumo todos los elementos de la matriz a ver si es 0

    if sum is 0:
        print("Es la matriz cero")

    else:
        mat = copiarMatriz(matriz)

        if cols>rows: #Veo si es una matriz rectangular
            index = 1 #Si es rectangular index me avisa
            while cols>rows:
                zeroes = []
                for i in range(cols): #Le agrego filas de 0's para
                    zeroes.append(0) #que la matriz pueda entrar al algoritmo
                mat.append(zeroes)
                rows += 1

        for i in range(rows):
            maxr = i
            for maxArg in range (i+1, rows):
                if abs(mat[maxArg][i])>abs(mat[maxr][i]): #Pivoteo
                    maxr=maxArg
                    (mat[i],mat[maxr])=(mat[maxr],mat[i])

            sum = 0
            for elmt in mat[i]:
                sum += abs(elmt)
            if (sum < 1/(10**14)): #Verifico si la fila en la que estoy es 0 o no
                break

            norm = mat[i][i] #Establezco una variable para evitar cambios
            for j in range(cols):
                mat[i][j] /= norm #Normalizo la fila

            for j in range(i+1,rows):
                rowMult = mat[j][i] #Defino el número por el cual multiplicar
                for k in range(i,cols):
                    mat[j][k] -= mat[i][k] * rowMult #Operación de fila
                    if abs(mat[j][k]) < 10**(-14):
                        mat[j][k]=0 #Evito errores de redondeo

        for i in range(rows-1, -1, -1):
            for j in range(i):
                rowMult = mat[j][i]
                for k in range(cols-1,i-1,-1):
                    mat[j][k] -= mat[i][k] * rowMult

        if index == 1:
            while rows > rowscopy:
                del mat[rows-1]
                rows -= 1

        return mat

def findInv (matriz):
    if determinant(matriz)!=0:
        rows = len(matriz)
        cols = len(matriz[0])
        inv = []
        mat = copiarMatriz(matriz)

        for i in range(rows): #Genero una matriz identidad
            medRow = []
            for j in range(cols):
                if i==j:
                    medRow.append(1)
                else:
                    medRow.append(0)
            inv.append(medRow)

        for i in range(rows):
            maxr = i
            for maxArg in range (i+1, rows):
                if abs(mat[maxArg][i])>abs(mat[maxr][i]):
                    maxr=maxArg
                    (mat[i],mat[maxr])=(mat[maxr],mat[i])
                    (inv[i],inv[maxr])=(inv[maxr],inv[i])

            norm = mat[i][i]
            for j in range(cols):
                inv[i][j] /=norm
                mat[i][j] /=norm

            for j in range(rows):
                rowMult = mat[j][i]
                for k in range(cols):
                    if i!=j:
                        inv[j][k] -= inv[i][k] * rowMult
                        mat[j][k] -= mat[i][k] * rowMult

        for i in range(rows-1, -1, -1):
            for j in range(i):
                rowMult = mat[j][i]
                for k in range(cols-1,i-1,-1):
                    inv[j][k] -= inv[i][k] * rowMult
                    mat[j][k] -= mat[i][k] * rowMult

        return (inv)

    else:
        print("La matriz que ha ingresado no tiene inversa")

def determinant (matriz):
    det = 0
    if (len(matriz)!=len(matriz[0])):
        print ("La matriz no es cuadrada")
    else:
        mat = copiarMatriz(matriz)
        det=1
        rows = len(mat)
        cols = len(mat[0])

        for i in range (rows):
            maxr = i

            for maxArg in range (i+1, rows):
                if abs(mat[maxArg][i])>abs(mat[maxr][i]):
                    maxr=maxArg

            if maxr!=i:
                (mat[i],mat[maxr])=(mat[maxr],mat[i])
                det*=-1

            for j in range (i+1 ,rows):
                if (mat[i][i]!=0):
                    data = mat[j][i]/mat[i][i]
                    for k in range(i,cols):
                        mat[j][k]-=mat[i][k]*data
                        if abs(mat[j][k])<(1/10**14):
                            mat[j][k]=0

        for i in range(rows):
            det*=mat[i][i]
        return det


if __name__ == '__main__':
    main()
